# README #

This repository holds the documentation for the Playbook Project. 

### What is this repository for? ###

* Sharing Training Materials
* Promoting Collaboration, "living documents"!
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Request access to Autodesk's Git account
* Request access to this repo
* ?
* Profit!

![how do we profit again?](https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.nerdfitness.com%2Fwp-content%2Fuploads%2F2012%2F04%2FScreen-Shot-2012-03-30-at-3.15.38-AM-1024x7591.jpg&f=1)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Ted Curran](mailto:ted.curran@autodesk.com)
* [Dave Sanchez](mailto:david.sanchez@autodesk.com)