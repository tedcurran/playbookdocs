# Proper Naming of Blocks and Components





Some blocks may contain components with similar titles. In cases like this, you must avoid having multiple items with redundant names. This messes up the page Table of Contents. 



See the screenshot below for an example of **WHAT NOT TO DO:**

 

![img](assets/image001.png)

 

![img](assets/image002.png)

 

- Should not use the word “VIDEO – “ in the title
- Title is duplicated and not required
- TOC needed to be cleaned up
- I also updated the paragraph above the video

---



## Example of Best Practices

In this improved version, the whole learning module is organized under one cohesive title, simplifying its visual appearance as well as the page Table of Contents and progress meter. 

 

![img](assets/image003.png)

 

### Authoring View

![design view of block naming best practices](assets/designview.jpg)

 

You can see in this image that the **Block** is named "Move to an Industry Collection", however, that's not what we're seeing in the visual presentation. The **Block** title is set to "do not display" (per our general practice of not displaying **Block** names.) 

The left-hand **component** inside this block, the video component, is also named "Move to an Industry Collection", and that's what we're seeing in the final design. 

The accordion **component** on the right has its title set to "do not display", providing a more coherent visual appearance and keeping the page Table of Contents from having duplicates. 



![do not display](assets/donotdisplay.jpg)