# Adapt Learning Playbook Project | Builder Training

![welcometoadapt](assets/welcometoadapt.png)

[TOC]

## Project Objective

IN Phase I of this initiative, we successfully converted our sales playbooks from their previous platform on VPlaybooks.com and rebuilding it in the [Adapt Learning](https://community.adaptlearning.org/) platform. Moving forward, we will be getting new playbook content directly from the vendor DSG and building playbooks in Adapt for the first time. 



The big objective for this project is to create an offline-friendly learning experience that looks great on mobile devices as well as desktop computers. Sales people will use these courses as a resource to guide their selling conversations, so we want to make them easy and attractive to use on any device.



This document will help you get familiar with the Adapt Learning platform, learn to produce content consistently by following our production guidelines, and it'll suggest best practices for building content in Adapt. 



## Pre- Course Work: Get Familiar with Adapt

### [About Adapt](https://www.adaptlearning.org/index.php/about/)

Get your bearings with a general introduction to what Adapt is all about. 

### [Adapt Showcase]([Adapt showcase](https://www.adaptlearning.org/index.php/adapt-showcase/))

This is a gallery of exemplary courses built on Adapt. Check out what can be done, and notice how other designers solve specific problems using the Adapt framework. 

### [Our Adapt Authoring Environment](gso-se-authoring-prod.ecs.ads.autodesk.com)

This is our teams workspace within Autodesk. James Sanderson or Dave Sanchez can give you access. 



### [Our Playbook Project Folder on Box](https://autodesk.box.com/s/2tjbsz9fj0jo1t56kawu2fsos62hy0y9)

This is where we can share any assets and resources internally to our group. 



<div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/uh5LnKaCndw?ecver=2" width="640" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>



### Key learning Docs

* [Authoring Tool: Creating a Course](https://github.com/adaptlearning/adapt_authoring/wiki/Creating-a-Course)
* [Creating Your First Course](https://github.com/adaptlearning/adapt_framework/wiki/Creating-your-first-course)
* [Core Plugins in the Adapt Learning Framework](https://github.com/adaptlearning/adapt_framework/wiki/Core-Plug-ins-in-the-Adapt-Learning-Framework)
* [Deploying and Testing Your Adapt Course](https://github.com/adaptlearning/adapt_framework/wiki/Deploying-and-testing-your-Adapt-course)
* [FAQ](https://github.com/adaptlearning/adapt_authoring/wiki/FAQ)


### Video tutorials

- [Creating responsive e-learning with Adapt](https://youtu.be/SrS_2vg-Iis) by Sven Laux


### Participate in the eLearning Community

Adapt is still a young product with a vibrant and helpful online community. If you get stuck, you can ask questions in the community and usually get prompt and thoughtful responses. Make sure you [sign up in the forum](https://community.adaptlearning.org/course/view.php?id=2), follow the [responsive eLearning authoring forum](https://community.adaptlearning.org/mod/forum/view.php?id=56), follow [the blog](https://www.adaptlearning.org/index.php/blog/) and/or [Twitter](http://www.twitter.com/adaptlearning), and just know that you can come back here at any time when you're stuck. 





## Course Work: Following our Production Guidelines

This section is where we collect the tool-specific best practices for designing and configuring components. 

### Building Blocks of Adapt — Mind your ABCs

**Pages** contain **Articles**, which contain **Blocks**, which contain **Components**. These ABC's form the basic building blocks of courses in Adapt. 



![The ABCs of Adapt](https://github.com/adaptlearning/documentation/raw/master/04_wiki_assets/adapt_framework/adapt-a-b-c.jpg)





#### Component Title Conventions

We always give titles to Pages, Articles, Blocks, and Components. However, for some items, we disable display of the title. Details on this practice follow:



Generally, we mark **Block** titles as "do not display". 

![donotdisplay](assets/donotdisplay.jpg)







Some blocks may contain components with similar titles. In cases like this, you must avoid having multiple items with redundant names. This messes up the page Table of Contents. 



See the screenshot below for an example of **WHAT NOT TO DO:**

 

![img](assets/image001.png)

 

![img](assets/image002.png)

 

- Should not use the word "VIDEO" in the title
- Title is duplicated and not required
- TOC needed to be cleaned up
- I also updated the paragraph above the video

---



####  Example of Best Practices

In this improved version, the whole learning module is organized under one cohesive title, simplifying its visual appearance as well as the page Table of Contents and progress meter. 

 

![img](assets/image003.png)

 

#### Authoring View

![design view of block naming best practices](assets/designview.jpg)

 

You can see in this image that the **Block** is named "Move to an Industry Collection", however, that's not what we're seeing in the visual presentation. The **Block** title is set to "do not display" (per our general practice of not displaying **Block** names.) 

The left-hand **component** inside this block, the video component, is also named "Move to an Industry Collection", and that's what we're seeing in the final design. 

The accordion **component** on the right has its title set to "do not display", providing a more coherent visual appearance and keeping the page Table of Contents from having duplicates. 



![do not display](assets/donotdisplay.jpg)

###  

Page Completion Criteria — Best Practices by Component Type

#### Media/Video

When adding a video in a Media component, set the completion to **inview**: 

![Video Completion](assets/video-inview.gif)



##### Video Encoding

Since all videos are bundled into the finished Playbook for offline viewing, we need to optimize videos for manageable file sizes for easier downloads.

All videos must be encoded as MPEG4 (.mp4) video, and run through [Handbrake](https://handbrake.fr/) with the **Fast1080p30** setting to ensure consistent quality and optimized file size. Ideally, no video should be larger than 50MB, while most videos should be under 20MB. If necessary, longer videos can be downgraded to 720p or even 480p resolution to meet this requirement. 



#### Accordions

For Title: Use something descriptive of the content

There's no hard-and-fast rule about page level progress on these — sometimes we track them, sometimes we don't. It depends on the content, so you have to make a judgement call. Ask one of us if you're in doubt. 

To disable Page Level Progress: click into the plugin and select "false. 

#### Tabs

There's no hard-and-fast rule about page level progress on these — sometimes we track them, sometimes we don't. It depends on the content, so you have to make a judgement call. Ask one of us if you're in doubt. 

To disable Page Level Progress: click into the plugin and select "false. 



#### Plain Text

There's no hard-and-fast rule about page level progress on these — sometimes we track them, sometimes we don't. It depends on the content, so you have to make a judgement call. Ask one of us if you're in doubt. 

To disable Page Level Progress: click into the plugin and select "false. 

#### 

#### Graphics/ Images

Make sure to tag each media asset with the name of the course. 

Please use explicitly descriptive titles, file names, and descriptions for images.

#### Expose



#### Hyperlinks

If you include hyperlinks to websites or files, you must format them so they [open in a new web window](https://www.w3schools.com/jsref/met_win_open.asp). Failing to do this will cause a conflict in Saba resulting in the user's progress not being recorded. 



#### Jumper Component

Each Jumper component has a field where you enter the Block ID that you want it to jump your viewers *to*. When building a new course from a template, this value will need to be changed. 

It is also possible to use the Jumper component to redirect users to another page within Adapt — a capability that is otherwise not possible. 







### Media Naming Conventions

- Tag media assets with full name of the course it appears in
- Keep original file name

#### To tag an asset:

1. enter "Edit" dialog on media asset
2. Paste Complete Course Name into "Tags" area. 
3. Add a comma "`,`" immediately after the course name to register it as a tag.




### Works in Progress

#### Closed Captions

We will need to discuss if/how we will include closed captioning in the videos. This has not come up yet but I expect we will need to get transcripts of the video assets and include CC files in the media element. Dave is figuring this out from a technical perspective and can share out best practices. 



#### Assessments

As we discussed in the kickoff meeting, there are no planned assessment items for the playbooks at the present time, but there may be the possibility in the future, so we're leaving this section here as a placeholder. 



#### Localization

Dave and James have done considerable work on localizing our first wave of content. We still need to understand and document this process so we can do better developing content with localization in mind. TBD.

---



## Tagging Courses

In the Course' Project Settings, go to Tags. 

Add tags for 

- Course name
- Author Name
- Completion Status
  - Not Started
  - In Progress
  - Complete

![course tagging](assets/course-tagging.gif)




## Copying Course Templates

1. We are using the course "Win More Bids" as a template for the Construction courses. 
2. To create a copy of this course to modify, do the following:
   1. Select gear on course 
   2. press Copy
   3. Save a new course with a new name and begin to modify!

![course copy](assets/course-copy.gif)





---



## Component Types — "See This, Build That"

I'm working on a video tutorial that shows how we translate elements from VPlaybook to Adapt. In the meantime, this is a simple translation table for matching content types to the Adapt tools you will likely want to use to display them. 



Here's also a handy table of elements we've been using in Adapt.

| Content Type | Adapt Component | Comments                                 |
| :----------- | :-------------- | :--------------------------------------- |
| Text         | Text            | Much of the content you'll convert is plain text. You can use the "Text" component to present it, or explore some of the more complex UI elements like Expose, Tabs, and others. |
| Image        | Graphic         |                                          |
| Video        | Media           | The "Media" component is the best for displaying video. |
| Tabs         | Tabs            | Explore whether top tabs or left tabs work best for each element |
| Preview      | Jumper          | Jumper can "jump" the user past certain content, or even to another page in your Adapt course. |
| Columns      | Expose, text    |                                          |
|              |                 |                                          |
|              |                 |                                          |
|              |                 |                                          |



## Build Tricks: VIDEO

### [Enable the Quick Navigation plugin](https://autodesk.box.com/s/mkgu598bmwr943mrje229mxtlfn3m5dv)

This video shows you how to add a nice navigation box to the bottom of your pages. 

### [Fixing Awkward Tab Blocks in **Sales Strategy** page](https://autodesk.box.com/s/mllzbliytzmxr9ljg1syeoj7r8xdf8kv)

Please see this video — it corrects a design flaw in our source courses that may show up in your newly created courses.

### [Bulleted Tables](https://autodesk.box.com/s/e8t96n1u1mtnjq8iidipn2kxv95pzeqt)

This is a quick video describing how to add bullets to all items in a table. 



## Approved Extensions

Adapt platform allows the use of **Extensions** — community-sourced plugins that extend the functionality of the platform. 

While there are many extensions available, we have only approved a small subset for use within our playbooks. Approved extensions are tested to be compatible with our Saba Cloud environment. 





## Approved Components

- Text
- Media
- Graphic
- Accordion
- Expose
- Tabs



## Components to Test

- [ ] Narrative
- [ ] Stack List

